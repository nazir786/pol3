/**
 * Created by Nazir on 24/4/16.
 */
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mysql = require("mysql");

var app = express();

//setup handlebars view engine
var exphbs = require('express-handlebars');
app.engine('.hbs', exphbs({defaultLayout: 'main', extname: '.hbs'}));
app.set('view engine', '.hbs');

app.set('port', process.env.PORT || 3000);

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, '/public/img', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/public')));

console.log("Current Directory --" + path.join(__dirname, 'public'));

//Load my dbconfig
//var config = require("./modules/dbconfig");

//Initialize mysql

//Create a connection pool
var connPool = mysql.createPool({
    connectionLimit: 5, //Create a pool of 5 connection
    host: "173.194.233.129", //CloudSQL IP address - get IPV4
    user: "nazir",
    password: "Fazaf1416",
    database: "pol_db"
});

console.log('MySQL pool created');

app.use(function(req, res, next) {
    res.locals.showTests = app.get('env') !== 'production' && 
        req.query.test === '1';
    
    next();
    
});

//routing starts here

app.get('/', function(req, res){
    "use strict";
    res.render('home');
});

app.get('/myGroup', function(req, res){
    "use strict";

    //Get a connection from the pool
    connPool.getConnection(function(err, conn) {
        //If there are errro, report it
        if (err) {
            console.error("get connection error: %s", err);
            res.render("error");
            return;
        }
        //Perform the query
        conn.query("SELECT consultantCode, consultantName FROM agents LIMIT 10", function(err, rows) {
            if (err) {
                console.error("query has error: %s", err);
                res.render("error");
                return;
            }

            conn.release();
            console.log('mySQL pool released');

            console.log('Rows returned : %s', rows);



            res.render('myGroup', {
                title: "Rushmore",
                agentsDetails: rows
            });
            //console.log('Returned data', JSON.stringify(rows));

        });



            // var agentDetails =  JSON.stringify(rows);
            // console.log(agentDetails);
            //
            // //Render the template with rows as the context
            // res.render('myGroup', agentDetails);
            // //IMPORTANT! Release the connection


    });

});

app.get('/about', function(req, res){
    "use strict";
    res.render('about');
});

app.get('/headers', function(req, res){
    "use strict";
    res.set('Content-Type', 'text/plain');
    var s = '';
    for (var name in req.headers) {
        s += name + ': ' + req.headers[name] + '\n';
    }
    res.send(s);

});

//custom 404 page
app.use(function(req, res){
    "use strict";
    res.status(404);
    res.render('error');
});

//custom 500 page
app.use(function(err, req, res, next){
    "use strict";
    console.error(err.stack);
    res.status(500);
    res.render('error');
});

app.listen(app.get('port'),function(){
    "use strict";
    console.log('Express started on http://localhost:' + 
        app.get('port') + '; press Ctrl-C to terminate.');
    
});




