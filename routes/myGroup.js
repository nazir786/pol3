"use strict";

var express = require('express');
var router = express.Router();

/* GET users listing. */

//Get the agent data from database
//Get the persons' name
//perform a database lookup of all reporting to him
//render("group_info", { reports: rows })

router.get('/', function (req, res) {

    var team = [
        { consultantCode: "30001",
            name: "Abraham Lincoln",
            afiMTD: 2000,
            afiYTD: 10000 },
        { consultantCode: "30002",
            name: "George Washington",
            afiMTD: 4000,
            afiYTD: 12000 },
        { consultantCode: "30003",
            name: "Theodore Roosevelt",
            afiMTD: 2500,
            afiYTD: 17000 },
    ];

    res.render('myGroup', team);
    console.log('myGroup');
});



module.exports = router;
