"use strict";

var express = require('express');
var router = express.Router();

/* GET home page. */
// '/investor' directory. Can't put /investor because require('express) resets the root to current directory which is /investor


console.log('Investors loaded');

router.get('/', function (req, res) {
    //Get the persons' name
    //perform a database lookup of all reporting to him
    //render("group_info", { reports: rows })
    res.render('investors');
    console.log('investors');
});

module.exports = router;
